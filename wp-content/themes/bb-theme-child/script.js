var $ = jQuery;

jQuery(document).ready(function() {

    var urlParams = new URLSearchParams(window.location.search);

    console.log(urlParams.has('ip')); // true
    console.log(urlParams.get('ip')); // "order"

    var ipadd = urlParams.get('ip');

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        dataType: "json",
        data: 'action=get_storelisting',

        success: function(response) {

            if (response) {

                console.log(response);

                //data = data.toJSON();

                var store = jQuery.parseJSON(JSON.stringify(response));

                jQuery(".header_store a.phone").attr('href', 'tel:' + store.phone);
                jQuery(".header_store a.phone span").html(store.formatphone);
                jQuery(".store-container .address").html('<a href="' + store.map_url + '" target="_blank"><p>' + store.address + '</p><p>' + store.city + ', ' + store.state + ' ' + store.postal_code + '</p></a>');
                jQuery(".footer-phone a.phone span").html(store.formatphone);
                jQuery(".footer-phone a.phone").prop('href', 'tel:' + store.phone);

                jQuery(".ajaxphoneretailer").html(store.formatphone);
                jQuery(".ajaxphoneretailer").prop('href', 'tel:' + store.phone);

                jQuery("#header-shop-at-home").show();

                jQuery(".responsive-shop-at-home").show();

                jQuery("#shop-at-home-popup").trigger("click");



            }
        }
    });

});